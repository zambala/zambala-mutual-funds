//
//  PageImageScrollView.m
//  PageimageScrollView
//
//  Created by Revo Tech on 6/15/16.
//  Copyright © 2016 Revo Tech. All rights reserved.
//

#import "PageImageScrollView.h"
@interface PageImageScrollView() <UIScrollViewDelegate>

@property(nonatomic) BOOL pageControlIsChangingPage;

@end

@implementation PageImageScrollView
#define PAGECONTROL_DOT_WIDTH 20
#define PAGECONTROL_HEIGHT 20
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
        self.pageControl = [[UIPageControl alloc] init];
        [self setDefaults];
        [self.pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:self.scrollView];
        [self addSubview:self.pageControl];
        self.scrollView.delegate = self;
     [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    }
    return self;
}


- (void)setPageControlPos:(enum PageControlPosition)pageControlPos
{
    CGFloat width = PAGECONTROL_DOT_WIDTH * self.pageControl.numberOfPages;
    
    
    _pageControlPos = pageControlPos;
    if (pageControlPos == PageControlPositionRightCorner)
    {
        self.pageControl.frame = CGRectMake(self.scrollView.frame.size.width -width, self.scrollView.frame.size.height - PAGECONTROL_HEIGHT, width, PAGECONTROL_HEIGHT);
    }else if (pageControlPos == PageControlPositionCenterBottom)
    {
        self.pageControl.frame = CGRectMake((self.scrollView.frame.size.width - width) / 2, self.scrollView.frame.size.height - PAGECONTROL_HEIGHT, width, PAGECONTROL_HEIGHT);
    }else if (pageControlPos == PageControlPositionLeftCorner)
    {
        self.pageControl.frame = CGRectMake(0, self.scrollView.frame.size.height - PAGECONTROL_HEIGHT, width, PAGECONTROL_HEIGHT);
    }
}

- (void)scrollingTimer {
    // access the scroll view with the tag
    //    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    //    // same way, access pagecontroll access
    //    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = self.scrollView.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/self.scrollView.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=4 )  {
        [self.scrollView scrollRectToVisible:CGRectMake(nextPage*self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:YES];
        self.pageControl.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [self.scrollView scrollRectToVisible:CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:YES];
        self.pageControl.currentPage=0;
    }
}


- (void)setDefaults
{
    self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:179/255.0 green:179/255.0 blue:179/255.0 alpha:1.0f];
    self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:11/255.0 green:122/255.0 blue:179/255.0 alpha:1.0f];
    

    self.pageControl.hidesForSinglePage = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.pageControlPos = PageControlPositionRightCorner;
    
}


- (void)setScrollViewContents: (NSArray *)images
{
    //remove original subviews first.
    for (UIView *subview in [self.scrollView subviews]) {
        [subview removeFromSuperview];
    }
    if (images.count <= 0) {
        self.pageControl.numberOfPages = 0;
        return;
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * images.count, self.scrollView.frame.size.height);
    for (int i = 0; i < images.count; i++) {
        if (i==0) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 20, self.scrollView.frame.size.width-100, self.scrollView.frame.size.height-50)];
            [imageView setImage:images[i]];
            [self.scrollView addSubview:imageView];
            imageView.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
            imageView.layer.shadowOffset = CGSizeMake(0, 2);
            imageView.layer.shadowOpacity = 1;
            imageView.layer.shadowRadius = 4.2;
            imageView.clipsToBounds = NO;


        }
        else if (i==1)
        {
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.frame.size.width * i+50, 20, self.scrollView.frame.size.width-100, self.scrollView.frame.size.height-50)];
        [imageView setImage:images[i]];
        [self.scrollView addSubview:imageView];
            imageView.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
            imageView.layer.shadowOffset = CGSizeMake(0, 2);
            imageView.layer.shadowOpacity = 1;
            imageView.layer.shadowRadius = 4.2;
            imageView.clipsToBounds = NO;

        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.frame.size.width * i+50, 20, self.scrollView.frame.size.width-100, self.scrollView.frame.size.height-50)];
            [imageView setImage:images[i]];
            [self.scrollView addSubview:imageView];
            imageView.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
            imageView.layer.shadowOffset = CGSizeMake(0, 2);
            imageView.layer.shadowOpacity = 1;
            imageView.layer.shadowRadius = 4.2;
            imageView.clipsToBounds = NO;

        
  
        }
            }
    self.pageControl.numberOfPages = images.count;
    //call pagecontrolpos setter.
    self.pageControlPos = self.pageControlPos;
}

- (void)changePage:(UIPageControl *)sender
{
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    self.pageControlIsChangingPage = YES;
    
}

#pragma scrollviewdelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.pageControlIsChangingPage) {
        return;
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    //switch page at 50% across
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControlIsChangingPage = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.pageControlIsChangingPage = NO;
}


@end
