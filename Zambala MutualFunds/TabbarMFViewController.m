//
//  TabbarMFViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "TabbarMFViewController.h"
#import "VCFloatingActionButton.h"


@interface TabbarMFViewController ()

@end

@implementation TabbarMFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    float X_Co = (self.view.frame.size.width - 70)/2;
    float Y_Co;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if(screenHeight>736)
    {
        
        Y_Co = self.view.frame.size.height - 91;
    }
    
    else
    {
        Y_Co = self.view.frame.size.height - 60;
        
    }
    
    
    
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,Y_Co, 70, 70) normalImage:[UIImage imageNamed:@"icon10Plus"] andPressedImage:[UIImage imageNamed:@"more"] withScrollview:self.dummyTable];
    
    
    self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"plusMenu1",@"plusMenu8",@"plusMenu7",@"plusMenu5",@"walletnew",@"plusMenu3",@"plusMenu2"];
    self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Exclusive Offers",@"Trades",@"Orders",@"Fund Transfer",@"Wallet",@"Messages",@"Profile Settings"];
    self.addButton.hideWhileScrolling = YES;
    self.addButton.delegate = self;
    
    self.dummyTable.delegate = self;
    self.dummyTable.dataSource = self;
    
    
    [self.view addSubview:self.addButton];
    
    
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%tu",indexPath.row];
    
    
    
    cell.imageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%tu",indexPath.row]];
    
    //    cell.imageView.image=[self.imageArray objectAtIndex:indexPath.row];
    return cell;
}


-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    
}

//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
//{
//    if(tabBarController.selectedIndex==2)
//    {
//        return NO;
//    }
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
