//
//  TabbarMFViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"

@interface TabbarMFViewController : UITabBarController<UITableViewDelegate,UITableViewDataSource,floatMenuDelegate,UITabBarDelegate,UITabBarControllerDelegate>
@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property UITableView * dummyTable;

@end
