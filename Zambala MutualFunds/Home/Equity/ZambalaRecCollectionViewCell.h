//
//  ZambalaRecCollectionViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 19/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZambalaRecCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;

@end
