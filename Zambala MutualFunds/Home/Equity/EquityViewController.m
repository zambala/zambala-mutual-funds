//
//  EquityViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 18/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EquityViewController.h"
#import "HMSegmentedControl.h"
#import "PageImageScrollView.h"
#import "ZambalaRecCollectionViewCell.h"
#import "EquityTableViewCell.h"
#import "EquityDetailViewController.h"



@interface EquityViewController ()
{
    HMSegmentedControl * segmentedControl;
     NSMutableArray * imageArray;
    NSMutableArray * testArray;
}

@end

@implementation EquityViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    testArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"All"];
    [localSegmentsArray addObject:@"Large Cap"];
    [localSegmentsArray addObject:@"Mid Cap"];
    [localSegmentsArray addObject:@"Small Cap"];
    [localSegmentsArray addObject:@"Multi Cap"];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 70, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.view addSubview:segmentedControl];
    
    
    [testArray addObject:@"Test 1"];
    [testArray addObject:@"Test 2"];
    [testArray addObject:@"Test 3"];
    
    self.pageControl.hidesForSinglePage=YES;
    
    self.topCollectionView.delegate=self;
    self.topCollectionView.dataSource=self;
    [self.topCollectionView reloadData];
    
    self.equityTableView.delegate=self;
    self.equityTableView.dataSource=self;
    [self.equityTableView reloadData];
    
    [self.topCollectionView reloadData];
    
 //   [self autoScroll];
    
    
//    PageImageScrollView *pageScrollView = [[PageImageScrollView alloc] initWithFrame:CGRectMake(0,0,self.pageView.frame.size.width,self.pageView.frame.size.height)];
//    //[pageScrollView setScrollViewContents:@[[UIImage imageNamed:@"Rounded Rectangle 23 copy.png"], [UIImage imageNamed:@"Rounded Rectangle 23 copy.png"], [UIImage imageNamed:@"Rounded Rectangle 23 copy.png"], [UIImage imageNamed:@"Rounded Rectangle 23 copy.png"]]];
//
//
//    //easily setting pagecontrol pos, see PageControlPosition defination in PagedImageScrollView.h
//    pageScrollView.pageControlPos = PageControlPositionCenterBottom;
//
//
//    [self.pageView addSubview:pageScrollView];
//
   // imageArray=[[NSMutableArray alloc]initWithObjects:@"travelStay.png",@"entertainment.png",@"shopping.png",@"food.png",@"homeLifestyle.png",@"sportsFitness.png",@"food.png", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onSegmentTap
{
    
}


//-(void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wisdomGradenTableView];
//    NSIndexPath *indexPath = [self.wisdomGradenTableView indexPathForRowAtPoint:buttonPosition];
//    [self.topCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
//}

- (void)viewDidLayoutSubviews
{
    [self.topCollectionView layoutIfNeeded];
    NSArray *visibleItems = [self.topCollectionView indexPathsForVisibleItems];
    NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:currentItem.section];
    
    [self.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}


//-(void)autoScroll{
//    CGFloat point = self.topCollectionView.contentOffset.x;
//    CGFloat lo = point + 1;
//    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        self.topCollectionView.contentOffset = CGPointMake(lo, 0);
//    }completion:^(BOOL finished){
//        if (self.testIndexPath==3)
//        {
//            [self autoScrollReverse];
//        }
//        else{
//            [self autoScroll];
//        }
//    }];
//}
//
//-(void)autoScrollReverse{
//    CGFloat point = self.topCollectionView.contentOffset.x;
//    CGFloat lo = point - 1;
//    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        self.topCollectionView.contentOffset = CGPointMake(lo, 0);
//    }completion:^(BOOL finished){
//        if(self.testIndexPath == 0){
//            [self autoScroll];
//        }else{
//            [self autoScrollReverse];
//
//        }
//
//    }];
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZambalaRecCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ZambalaRecCollectionViewCell" forIndexPath:indexPath];
    cell.investNowButton.layer.cornerRadius=15.0f;
    cell.investNowButton.layer.masksToBounds = NO;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
   // self.pageControl.currentPage = indexPath.row;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.topCollectionView.frame.size.width;
    self.pageControl.currentPage = self.topCollectionView.contentOffset.x / pageWidth;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     EquityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"EquityTableViewCell" forIndexPath:indexPath];
    cell.investNowButton.layer.cornerRadius=15.0f;
    cell.investNowButton.layer.masksToBounds = NO;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
    [self.navigationController pushViewController:detail animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
