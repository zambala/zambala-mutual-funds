//
//  EquityTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;

@end
