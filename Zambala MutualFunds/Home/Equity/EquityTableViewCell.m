//
//  EquityTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EquityTableViewCell.h"

@implementation EquityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
