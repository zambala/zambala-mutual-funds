//
//  HomeViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ViewController.h"

@interface HomeViewController : ViewController
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;
@property (weak, nonatomic) IBOutlet UILabel *fundsRecommendedLabel;
@property (weak, nonatomic) IBOutlet UIButton *equityButton;
@property (weak, nonatomic) IBOutlet UIButton *debtButton;
@property (weak, nonatomic) IBOutlet UIButton *hybridbutton;
@property (weak, nonatomic) IBOutlet UIButton *taxSaverButton;
@property (weak, nonatomic) IBOutlet UIButton *smartMoneyButton;
@property (weak, nonatomic) IBOutlet UIButton *othersButton;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *fundsPickedButton;
@property (weak, nonatomic) IBOutlet UILabel *fundsPickedLabel;

@end
